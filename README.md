# What
Automatically send a heartbeat every 60 seconds to the Kei Jidosha service registry. Is dependant on the global variable _Kei
```
{
  "appId": "string",
  "instanceId": "string",
  "endpoint": "string:link"
}
```

# How
Install the package and require it in your application startup.
