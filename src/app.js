const request = require("request")

let _retries = 0
const _maxRetries = 60

const heartbeat = (cfg) => {
  setInterval(() => {
    try {
      request({
        uri: `${cfg.registryEndpoint}/applications/${cfg.appId}/instances/${cfg.instanceId}/heartbeat`,
        method: "POST",
      })
    } catch(err) {
      console.log(`Failure sending hearbeat for app ${cfg.appId} on instance ${cfg.instanceId} with error`, err)
    }
  }, 60000)
}

const bootUp = () => {
  setTimeout(() => {
    if (_retries >= _maxRetries) {
      console.error("Failed to connect to Kei Jidoshi service registry, no valid config found")
      return null //die
    }

    if (!global._Kei || !global._Kei.appId || !global._Kei.instanceId || !global._Kei.registryEndpoint) {
      _retries++

      bootUp()
      return null
    }

    heartbeat(global._Kei)
  }, 1000)
}

bootUp()
